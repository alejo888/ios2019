import UIKit

protocol User {
    var name: String{get set}
    func status()
    func showAds()
}

class Normal: User {
    var name: String
    
    init(NormalName normalname : String) {
        self.name = normalname
    }
    
    func status(){
        print("\(name) is a Normal User")
    }
    
    func showAds(){
        print("The User \(name) sees ads")
    }
}

class Premium: User {
    var name: String
    
    init(PremiumName premiumname : String) {
        self.name = premiumname
    }
    
    func status(){
        print("\(name) is a Premium User")
    }
    
    func showAds(){
        print("The User \(name) doesn't see ads")
    }
}

enum userType{
    case Normal
    case Premium
}

class userFactory {
    private static var sharedUserFactory = userFactory()
    
    class func shared()-> userFactory {
        return sharedUserFactory
    }
    func getUser(UserFactory userType : userType, UserName userName : String) -> User {
        switch userType {
        case .Normal:
            return Normal(NormalName: userName)
        case .Premium:
            return Premium(PremiumName: userName)
        }
    }
}

let normal = userFactory.shared().getUser(UserFactory: .Normal, UserName: "Alejandro")
normal.status()
normal.showAds()

let premium = userFactory.shared().getUser(UserFactory: .Premium, UserName: "Daniel")
premium.status()
premium.showAds()
